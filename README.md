## About Tambola

It's really simple project to pick random movies from list.

**Technology Used**
1. HTML.
2. CSS.
3. Bootstrap.
4. Font Awesome
5. jQuery.

---

## How to Run Site

It's totally upto you, I usually deploy using IIS.
This time I have used GoLive extension from Visual Studio Code.

## Info
Currently movies.json is not being used.